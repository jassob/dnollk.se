DNollK.se
====================
DNollK Chalmers hemsida
-------------------
Detta kommer till slut, i sinom tid, kanske, förhoppningsvis bli en hemsida som är klar före jul.
Absolut göttigast hade vart att ha den klar till mottagningen 2015.

För de olika branches som finns så leder

 * php-old till  DNollK '14s sida
 * design till en ren html/css implementation av designen
 * ruby till en eventuell ruby/sinatra/rails-implementation
 * haskell till en eventuell haskell/yesod-implementation
 * [python](https://github.com/Jassob/DNollK.se/tree/python) till en python/django-implementation
 

OBS, python-branchen är troligen den som vi kommer koda på (därav länken).
 
För mer information på de olika implementationerna hänvisas man till respektive branch.
